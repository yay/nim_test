# https://en.wikipedia.org/wiki/Region-based_memory_management
# AFAIK, it means that you do not count individual allocations but just alloc everything
# from a block of memory. After leaving the local area you throw away everything.
# Stuff you want to keep is copied before the destruction.

# The types ref and ptr can get an optional region annotation. A region has to be an object type.
# Regions are very useful to separate user space and kernel memory in the development of OS kernels.

type
  Kernel = object
  Userspace = object

var a: Kernel ptr Stat
var b: Userspace ptr Stat

# The following does not compile as the pointer types are incompatible:
# a = b

# Same:
# region ptr T
# ptr[region, T]