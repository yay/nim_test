# https://news.ycombinator.com/item?id=9049965

# https://nim-lang.org/docs/manual.html#types-reference-and-pointer-types

# traced references = normal references, point to objects of a garbage collected heap
# untraced references = pointers, point to manually allocated objects somewhere else in memory

# Keywords:
# traced - 'ref'
# untraced - 'ptr'

# In general, 'ptr T' is implicitly convertible to the 'pointer' type.

# Dereference - '[]' empty subscript notation
# Address - 'addr' procedure, the address is always an untraced reference

# Perform implicit dereference:
# Access a tuple/object field - '.'
# Array/string/sequence index - '[]'

type
  NodeObj = object
    le, ri: Node
    data: int
  Node = ref NodeObj
  NodePtr = ptr NodeObj

  MyTuple = tuple[a: int, b: string]
  Data = tuple[x, y: int, s: string]

  # Recursive type definitions are not allowed, but this works:
  MyNode = ref object  # 'prt object' also works
    le, ri: MyNode
    data: int

var
  n: Node
  np: NodePtr
  t: MyTuple

new(n)  # Allocate a new traced object.
n.data = 9  # No need to write n[].data.

t.a = 5
t.b = "Hello"

echo t.b, " ", t.a

# alloc, dealloc, realloc - deal with untraced memory

# Allocate memory for Data on the heap:
var d = cast[ptr Data](alloc0(sizeof(Data)))
np = cast[NodePtr](alloc0(sizeof(NodeObj)))

# 'alloc0' returns an untyped pointer, which we cast to the pointer type 'ptr Data'.
# Casting should only be done if it is unavoidable: it breaks type safety and bugs
# can lead to mysterious crashes.

# Note: The example only works because the memory is initialized to zero
# (alloc0 instead of alloc does this): d.s is thus initialized to nil
# which the string assignment can handle.
# One needs to know low level details like this when mixing garbage collected
# data with unmanaged memory.

d.s = "abc"  # Create a new string on the garbage collected heap.

echo d.s

GC_unref(d.s)  # Tell the GC that the string is not needed anymore.
               # Without this call, the memory allocated for d.s would never be freed,
               # since d.s is an untraced reference.

echo d.s  # TODO: could be garbage collected already at this point?

dealloc(d)  # free the memory
dealloc(np)

var ms = newStringOfCap(10)
echo ms, " ", ms.len  # capacity is 10, but the length is 0
# ms[10] = '!'  # index out of bounds (runtime check/error)
ms = "Hello World!"  # works fine, TODO: but have the 10 allocated bytes leaked?
echo ms, " ", ms.len  # ms.len is 12


# https://nim-lang.org/docs/manual.html#types-not-nil-annotation

type
  PObject = ref NodeObj not nil
  TProc = (proc (x, y: int)) not nil

proc p(x: PObject) =
  echo "not nil"

# Compiler catches this:
# p(nil)
# The compiler ensures that every code path initializes variables
# which contain non nilable pointers.


# https://nim-lang.org/docs/manual.html#types-memory-regions

var s = cast[ptr string](alloc0(sizeof(10)))
echo s[]
s[] = "hello"
echo s[], " ", s[].len
dealloc(s)

# Allocate thread local heap memory
var a = alloc(1000)
dealloc(a)
 
# Allocate memory block on shared heap
var b = allocShared(1000)
deallocShared(b)
