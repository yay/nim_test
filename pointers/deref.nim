var
  a: int = 3
  b: ptr int = addr a
  c: ptr ptr int = addr b
  d: ptr ptr ptr int = addr c

echo repr(d)
echo repr(c)
echo repr(b)
echo repr(a)