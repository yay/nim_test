#include <stdio.h>

// What does it actually compile into?
// t = b * c
// z = a + t
// versus
// z = a + b * c

int main()
{
    int a = 5;
    int b = 10;
    int c = 15;
    int z = a + b * c;
    printf("The result is %d\n", z);
    return 0;
}