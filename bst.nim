type
  BNode = ref BNodeObj
  BNodeObj = object
    ln: BNode
    rn: BNode
    data: int

  BTree = ref BTreeObj
  BTreeObj = object
    root: ref BNodeObj

# Recursive node version.
proc find_r(root: BNode, data: int): bool =
  if root == nil:
    return false
  elif root.data == data:
    return true
  else:
    if root.data < data:
      discard find_r(root.rn, data)
    else:
      discard find_r(root.ln, data)

# Recursive tree version.
proc find_r(tree: BTree, data: int): bool =
  return find_r(tree.root, data)

# Non-recursive tree version.
proc find(tree: BTree, data: int): bool =
  var it = tree.root

  while it != nil:
    if it.data == data:
      return true
    else:
      it = if it.data < data: it.rn else: it.ln

  return false

# 'var' is used on the return parameter as well to make it mutable
# and allow for method chaining, e.g.:
#
#   var node: BNode = nil
#   discard node.insert_r(3).insert_r(1).insert_r(2)
#
proc insert_r(root: var BNode, data: int): var BNode =
  if root == nil:
    new root
    root.data = data
  elif root.data == data:
    return root
  else:
    if root.data < data:
      discard insert_r(root.rn, data)
    else:
      discard insert_r(root.ln, data)

  return root

# Non-recursive insert version.
proc insert(root: var BNode, data: int): bool =
  if root == nil:
    new root
    root.data = data
  else:
    var it = root

    while true:
      if it.data == data:  # by removing the test for equality
        return false       # we can allow for duplicates
      else:
        if it.data < data:
          if it.rn == nil:
            new it.rn
            it.rn.data = data
            break
          it = it.rn
        else:
          if it.ln == nil:
            new it.ln
            it.ln.data = data
            break
          it = it.ln

  return true

proc remove(root: var BNode, data: int): bool =
  return false

#[
  When to use Pre-Order, In-order or Post-Order?
  Pick the strategy that brings you the nodes you require the fastest.
  1. If you know you need to explore the roots before inspecting any leaves, you pick pre-order
  because you will encounter all the roots before all of the leaves.
  2. If you know you need to explore all the leaves before any nodes, you select post-order
  because you don't waste any time inspecting roots in search for leaves.
  3. If you know that the tree has an inherent sequence in the nodes, and you want to flatten
  the tree back into its original sequence, than an in-order traversal should be used.
  The tree would be flattened in the same way it was created. A pre-order or post-order
  traversal might not unwind the tree back into the sequence which was used to create it.
]#

proc preOrder(root: BNode) =
  if root != nil:
    echo root.data
    preOrder(root.ln)
    preOrder(root.rn)

proc inOrder(root: BNode) =
  if root != nil:
    inOrder(root.ln)
    echo root.data
    inOrder(root.rn)

proc postOrder(root: BNode) =
  if root != nil:
    postOrder(root.ln)
    postOrder(root.rn)
    echo root.data


var
  linearRoot: BNode  # created from linear sequence
  randomRoot: BNode  # created from random sequence

for i in countup(1, 10):
  discard linearRoot.insert_r(i)

var
  randomSeq: seq[int] = @[5, 3, 1, 8, 2, 10, 6, 4, 9, 7]

for _, el in randomSeq:
  discard randomRoot.insert(el)

# inOrder(linearRoot)
# inOrder(randomRoot)

echo repr(linearRoot)
# echo repr(randomRoot)

