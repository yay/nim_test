import winim

proc getSystemDirectory*(): string =
  let n = GetSystemDirectory(nil, 0)
  let str = cast[LPWSTR](alloc(n))
  GetSystemDirectory(str, n)
  result = $str
  dealloc(str)

echo getSystemDirectory()