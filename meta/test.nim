# https://hookrace.net/blog/introduction-to-metaprogramming-in-nim/

template genType(name, fieldname: untyped, fieldtype: typedesc) =
  type
    name = object
      fieldname: fieldtype

type
  Testing = object
    bar: string

genType(Test, foo, int)

var x = Test(foo: 4566)
var y = Testing(bar: "hello")
echo(x.foo) # 4566
echo(y.bar) # hello