import strutils

type
    Dollar = distinct int
    Euro = distinct int

# proc `+` (x, y: Dollar): Dollar =
#     result = Dollar(x.int + y.int)

# proc `*` (x: Dollar, y: int): Dollar =
#     result = Dollar(x.int * y)

# proc `*` (x: int, y: Dollar): Dollar =
#     result = Dollar(x * y.int)

# proc `div` (x: Dollar, y: int): Dollar =
#     result = Dollar(x.int / y)

# we can rewrite the above using the 'borrow' pragma

proc `+` (x, y: Dollar): Dollar {.borrow.}
proc `*` (x: int, y: Dollar): Dollar {.borrow.}
proc `*` (x: Dollar, y: int): Dollar {.borrow.}
proc `div` (x: Dollar, y: int): Dollar {.borrow.}

# The borrow pragma makes the compiler use the same implementation
# as the proc that deals with the distinct type's base type, so no
# code is generated.

var
    d: Dollar = 10.Dollar
    e: Euro

echo int(5.Dollar + d)
echo int(d div 2)
