# Used to work differently, now produces the same output.
echo 2+2 * 5
echo 2 + 2 * 5

var b1: byte = 100
var b2: byte = 200  # 400 - 256

echo b1 shl 1  # 200
echo b2 shl 1  # (200 * 2) - 256 = 144