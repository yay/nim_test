proc print_signed(n: int) =
  echo n

proc print_unsigned(n: uint) =
  echo n

# print_signed(-1)
# print_unsigned(-1)  # compiler error