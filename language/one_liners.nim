
# http://blog.ubergarm.com/10-nim-one-liners-to-impress-your-friends/

import sequtils, strutils, math, future

echo toSeq(1..10).mapIt(it * 2)

echo toSeq(1..1000).foldl(a + b)
echo toSeq(1..1000).sum()

var wordList = @["nim", "nimlang", "nimrod", "nimble", "koch"]
var tweet = "This is an example tweet talking about nim and nimble."
echo wordList.mapIt(tweet.contains(it)).anyIt(it == true)

echo readFile("language/data.txt")
echo toSeq(lines("language/data.txt"))

echo lc[("Happy Birthday " & (if x != 3: "to you" else: "dear Ubergarm")) | (x <- 1..4), string]

var vals = @[49, 58, 76, 82, 88, 90]
var (passed, failed) = (filterIt(vals, it > 60), filterIt(vals, it <= 60))
echo passed
echo failed

# import httpclient, htmlparser, streams
# var client = newHttpClient()
# var html = parseHtml(newStringStream(client.getContent("http://google.com")))
# echo repr html

echo(@[14, 35, -7, 46, 98].foldl(min(a,b)))
echo(@[14, 35, -7, 46, 98].foldl(max(a,b)))
echo(@[14, 35, -7, 46, 98].min())
echo(@[14, 35, -7, 46, 98].max())