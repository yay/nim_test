# aka dynamic arrays
# https://nim-by-example.github.io/seqs/

var
  a = @[1, 2, 3]
  b = newSeq[int](3)

echo a
echo b

for index, value in a:
  b[index] = value * value

echo b

for i in 4..10:
  b.add(i * i)

echo b

b.delete(0)  # takes O(n) time
echo b

b = a[0] & b  # same as original b
echo b