import algorithm
from future import `=>`

proc takeProc(p: proc (x, y: int): int): int =
  result = p(5, 5)

echo takeProc(proc (x, y: int): int = x * y)
echo takeProc((x, y) => x + y)
