import options

proc find(haystack: string, needle: char): Option[int] =
  for i, c in haystack:
    if c == needle:
      return some(i)
  return none(int)  # this line is optional

try:
  assert("abc".find('c').get() == 2)  # Immediately extract the value
except UnpackError:  # If there is no value
  assert false  # This will not be reached, because the value is present