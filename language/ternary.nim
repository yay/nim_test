proc isOdd(n: int): bool =
  result = n mod 2 != 0

var x: string = if isOdd(5): "Yes" else: "No"
var y: bool = isOdd(4)

echo x
echo y