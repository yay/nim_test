# http://qiita.com/takaomag/items/145f0dfd191e3387917e#lexical-analysis

var
  i = 3
  i8 = 3'i8
  i16 = 3'i16
  i32 = 3'i32
  i64 = 3'i64
  
  ui = 3'u
  ui8 = 3'u8
  ui16 = 3'u16
  ui32 = 3'u32
  ui64 = 3'u64

echo i, " ", sizeof(i)
echo i8, " ", sizeof(i8)
echo i16, " ", sizeof(i16)
echo i32, " ", sizeof(i32)
echo i64, " ", sizeof(i64)