{.push stackTrace:off.}
proc addInt(a, b: int): int =
  # a in eax, and b in edx
  asm """
      mov eax, `a`
      add eax, `b`
      jno theEnd
      call `raiseOverflow`
    theEnd:
  """
{.pop.}

# {.push stackTrace:off.}
# proc addInt(a, b: int): int =
#   asm """
#     addl %%ecx, %%eax
#     jno 1
#     call `_raiseOverflow`
#     1:
#     :"=a"(`result`)
#     :"a"(`a`), "c"(`b`)
#   """
# {.pop.}

echo addInt(5, 3)