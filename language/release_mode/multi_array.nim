import math

# compile using:
# nim c -d:release multi_array.nim

# 'time' command is used, (user + sys) time is compared.

# Case 1:
# 34% slower (in release mode) than C code (debug target),
# when the following Nim compiler flags are used:
# --rangeChecks:off
# --boundChecks:off
# --deadCodeElim:on
# --opt:speed

# Case 2:
# 37% slower (in release mode) than C code (debug target),
# when the following Nim compiler flags are used:
# --rangeChecks:on
# --boundChecks:on
# --deadCodeElim:on
# --opt:speed

# Case 3:
# 3.36x slower (in debug mode) than C code (debug target) with no Nim compiler flags used.

# C code is tested with debug target because release target optimizes the code, so that
# the compiled app runs instantly.
# However, if the `printf("%d\n", a[70000][7000]);` line is added to print the value of
# a cell in the populated array, the Nim code in Case 1 is 2.82x slower instead of 34% slower.

# C code used:
#
#     int x = 100000;
#     int y = 10000;
#
#     int (*a)[y] = malloc(x * y * sizeof(int));
#
#     for (int ix = 0; ix < x; ix++) {
#         for (int iy = 0; iy < y; iy++) {
#             a[ix][iy] = iy;
#         }
#     }
#
#     printf("%d\n", a[70000][7000]);  // optional
#
#     free(a);

const
    x = 100_000
    y = 10_000

proc main() =

  var a = newSeq[float64](x * y)

  for ix in 0..<x:
    for iy in 0..<y:
      a[ix * y + iy] = if iy mod 2 == 0: sin(iy.float64) else: cos(iy.float64)

main()

# Nim automatically allocates the array on the heap,
# since the array is too large. One has to manually
# do this in C to avoid a segfault.
# If this is inside the `main` proc, though, it will cause a stack overflow.
# From Araq:
# and your OS performs memory overcommitment so that it fails in a stupid inconsistent manner.
# you can praise Unix's fork() call for that
# keeping operating systems from operating since 1970.

# var a: array[0..x, array[0..y, int]]

# for xi in 0..x:
#   for yi in 0..y:
#     a[xi][yi] = yi

# echo a[70000][7000]

# const
#   x = 100_000
#   y = 10_000

# const
#   x = 10
#   y = 10

# This is about 10% slower than the array version.
# var b = newSeq[int](x * y)

# for ix in 0..<x:
#   for iy in 0..<y:
#     b[ix * y + iy] = iy
  #   stdout.write b[ix * y + iy], " "
  # stdout.write "\n"

# for _, v in a:
#   echo repr(v)
