type
  Person = ref object of RootObj
    name*: string  # the * means that `name` is accessible from other modules
    age: int       # no * means that the field is hidden from other modules

  Student = ref object of Person # Student inherits from Person
    id: int                      # with an id field

var
  student: Student
  person: Person

assert(student of Student) # is true

# object construction:
student = Student(name: "Vitaly", age: 32, id: 8)
echo student[]