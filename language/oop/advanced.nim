type Animal = ref object of RootObj
    name: string
    age: int
method vocalize(this: Animal): string {.base.} = "..."
method ageHumanYears(this: Animal): int {.base.} = this.age

type Dog = ref object of Animal
method vocalize(this: Dog): string = "woof"
method ageHumanYears(this: Dog): int = this.age * 7

type Cat = ref object of Animal
method vocalize(this: Cat): string = "meow"

var animals: seq[Animal] = @[]
animals.add(Dog(name: "Sparky", age: 10))
animals.add(Cat(name: "Mitten", age: 10))

for a in animals:
    echo a.vocalize()
    echo a.ageHumanYears()

echo(animals[0] of Dog)
echo(animals[0] of Cat)
# echo(animals[0] of Animal)  # compiler warning: condition is always true

type RefObj = ref object
    greeting: string
type Obj = object
    greeting: string

proc newRefObj(greeting: string = "hello"): RefObj =
    new result
    result.greeting = greeting

proc makeObj(greeting: string = "hi"): Obj =
    result.greeting = greeting

let o1 = newRefObj("how are you doing?")
let o2 = makeObj("how do you do?")

echo o1.greeting
echo o2.greeting