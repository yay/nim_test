import math

let almostOne = 0.99999999999999

echo almostOne
echo uint8(almostOne * 255) # 254
echo uint8(almostOne * 255.99) # 255
echo round(almostOne * 255)  # floating point value!
let n = -1
let m = -2
echo uint8(n)
echo uint8(m)