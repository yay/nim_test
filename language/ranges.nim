import sequtils
import algorithm
import future

var
  hiInt = high(int)
  hiIntM1 = hiInt - 1
  hiIntM2 = hiInt - 2

# echo hiInt, "\n"
# echo start

when sizeof(int) <= 2:
  type IntLikeForCount = int|int8|int16|char|bool|uint8|enum
else:
  type IntLikeForCount = int|int8|int16|int32|char|bool|uint8|uint16|enum

template myCountupImpl(incr: untyped) {.dirty.} =
  when T is IntLikeForCount:
    var res = int(a)
    while res < int(b):
      yield T(res)
      incr
    yield T(res)
  else:
    var res: T = T(a)
    while res <= b:
      yield res
      incr

iterator `$$`*[S, T](a: S, b: T): T {.inline.} =
  myCountupImpl:
    inc(res)

# for i in hiIntM2..hiIntM1:
#   echo i

# var i = high(int) - 2
# while i < high(int):
#   echo i
#   inc i
# echo i

for i in hiIntM2$$hiInt:  # doesn't fail
  echo i

echo ""

for i in hiInt$$hiInt:
  echo i

echo ""

for i in hiInt$$0:
  echo i

var n = hiInt
echo n + 1

# for i in hiIntM2..hiInt:  # fails
#   echo i

# var cities = @["Frankfurt", "Tokyo", "New York", "Kyiv"]

# cities.sort((x, y) => x.len - y.len)

# echo cities