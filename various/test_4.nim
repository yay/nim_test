var x = 5
var y = "foo"

# x = y # compiler error

var z = int(1.0 / 3)

var f = "Foobar"

# cast keyword is unsafe and should be used only where you know what you are doing,
# such as in interfacing with C
proc ffi(foo: ptr array[6, char]) = echo repr(foo)
ffi(cast[ptr array[6, char]](addr y[0]))
