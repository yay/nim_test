# https://github.com/nim-lang/Nim/wiki/Nim-for-C-programmers

# int a = 3
# int *b = &a;

var a = 3.5
var b = addr a
echo repr(b)

# int x = 42;
# void *p = &x;
# int y = (int) *p;
# printf("%d, %d, %p\n", x, y, p);

var 
  x = 42
  p: pointer = addr x
  y = cast[ptr int](p)[]
echo x, " ", y, " ", repr(p)

# Nim arrays can be indexed from any number.
proc foobar(z: array[1..4, int]) =  # var array[0..3, int] if you mean to change it
  echo repr(z)

proc foo(x: var int): int =
  echo x
  result = x * 2

var arr = [1, 2, 3, 4]
foobar(arr)

var i = 5
echo foo(i)

# void foobar(person_t *a) {
#   person_t b;
#   b = *a;
#   b.name = "Bob";
#   *a = b;
# }

# proc foobar(a: ref TPerson) =
#   var b: TPerson
#   b = a[]
#   b.name = "Bob"
#   a[] = b