import os
import ospaths
import strutils

const nimExt = ".nim"
const nimCache = "nimcache"

let appDir = getAppDir()
let appFile = getAppFilename()

proc removeJunk(dir: string, level: int = 0) =

  let indent: string = " ".repeat(level * 2)

  for kind, path in walkDir(dir):

    case kind

    of pcLinkToFile, pcLinkToDir: discard

    of pcFile:
      echo indent, "File: ", extractFilename(path)
      # remove the executable (e.g. 'app') if the source (e.g. 'app.nim') was found
      if fileExists(path & nimExt) and path != appFile:
        echo "Removing file: ", path
        removeFile(path)

    of pcDir:
      let dirName = extractFilename(path)
      echo indent, "Dir: ", dirName
      if dirName == nimCache:
        echo "Removing directory: ", path
        removeDir(path)
      else:
        removeJunk(path, level + 1)
      

removeJunk(appDir)