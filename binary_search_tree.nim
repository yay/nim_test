# Source: http://www.eternallyconfuzzled.com/tuts/datastructures/jsw_tut_bst1.aspx

type
  BNode = ref BNodeObj
  BNodeObj = object
    link: array[0..1, BNode]
    data: int

  BTree = ref BTreeObj
  BTreeObj = object
    root: ref BNodeObj

# Recursive node version.
proc find_r(root: BNode, data: int): bool =
  if root == nil:
    return false
  elif root.data == data:
    return true
  else:
    let dir = int(root.data < data)
    return find_r(root.link[dir], data)

# Recursive tree version.
proc find_r(tree: BTree, data: int): bool =
  return find_r(tree.root, data)

# Non-recursive tree version.
proc find(tree: BTree, data: int): bool =
  var it: BNode = tree.root

  while it != nil:
    if it.data == data:
      return true
    else:
      let dir = int(it.data < data)
      it = it.link[dir]

  return false

proc insert_r(root: var BNode, data: int): BNode =
  if root == nil:
    new root
    root.data = data
  elif root.data == data:
    return root
  else:
    let dir = int(root.data < data)
    discard insert_r(root.link[dir], data)

  return root


# Non-recursive insert version.
proc insert(root: var BNode, data: int): bool =
  if root == nil:
    new root
    root.data = data
  else:
    var it = root
    var dir: int

    while true:
      if it.data == data:  # by removing the test for equality
        return false       # we can allow for duplicates
      else:
        dir = int(it.data < data)
        var child = it.link[dir]
        if child == nil:
          new child
          child.data = data
          it.link[dir] = child
          break;
        it = child

  return true

#[

To remove the root

           5

       /       \

     2           8

   /   \       /   \

 1       4   7       9

Copy 7 (inorder successor) to 5
Note: copying 4 (inorder predecessor) to 5 also works

The inorder predecessor of a node never has a right child,
and the inorder successor never has a left child.
So we can be sure that the successor always has at most one child,
and the left child is a leaf.

To find the successor simply walk once to the right,
then go all the way to the left.

           7

       /       \

     2           8

   /   \       /   \

 1       4   7       9

 Remove the external 7

           7

       /       \

     2           8

   /   \       /   \

 1       4   ~       9

]#

proc removeNode(root: var BNode, data: int): bool =
  if root != nil:
    var it = root
    var p: BNode

    while true:
      if it == nil:
        return false
      elif it.data == data:
        break

      p = it
      it = it.link[int(it.data < data)]

    # 'it' - the node being deleted
    # 'p' - its parent (if nil then we are deleting the root)
    # 'succ' - its inorder successor

    if it.link[0] != nil and it.link[1] != nil:
      # node has two children -> need to find the successor
      p = it
      var succ = it.link[1]

      while succ.link[0] != nil:
        p = succ
        succ = succ.link[0]

      it.data = succ.data
      p.link[int(p.link[1] == succ)] = succ.link[1]
      # free(succ)
    else:
      # node is an external node - only has one child
      let dir = int(it.link[0] == nil)
      if p == nil:
        root = it.link[dir]
      else:
        p.link[int(p.link[1] == it)] = it.link[dir]
      # free(it)

  return true


proc remove(root: var BNode, data: int): bool =
  if root != nil:
    var head = new BNode
    var it = head
    var p, f: BNode
    var dir = 1

    it.link[dir] = root
    while it.link[dir] != nil:
      p = it
      it = it.link[dir]
      dir = int(it.data <= data)

      if it.data == data:
        f = it  # found node, but keep going till external node is reached

    if f != nil:
      f.data = it.data
      p.link[int(p.link[1] == it)] = it.link[int(it.link[0] == nil)]
      # free(it)

    root = head.link[1]

  return true

#[
  When to use Pre-Order, In-order or Post-Order?
  Pick the strategy that brings you the nodes you require the fastest.
  1. If you know you need to explore the roots before inspecting any leaves, you pick pre-order
  because you will encounter all the roots before all of the leaves.
  2. If you know you need to explore all the leaves before any nodes, you select post-order
  because you don't waste any time inspecting roots in search for leaves.
  3. If you know that the tree has an inherent sequence in the nodes, and you want to flatten
  the tree back into its original sequence, than an in-order traversal should be used.
  The tree would be flattened in the same way it was created. A pre-order or post-order
  traversal might not unwind the tree back into the sequence which was used to create it.
]#

proc preOrder(root: BNode) =
  if root != nil:
    echo root.data
    preOrder(root.link[0])
    preOrder(root.link[1])

proc inOrder(root: BNode) =
  if root != nil:
    inOrder(root.link[0])
    echo root.data
    inOrder(root.link[1])

proc postOrder(root: BNode) =
  if root != nil:
    postOrder(root.link[0])
    postOrder(root.link[1])
    echo root.data


var
  linearRoot: BNode  # created from linear sequence
  randomRoot: BNode  # created from random sequence
  singleRoot: BNode

for i in countup(1, 10):
  discard linearRoot.insert_r(i)

var
  randomSeq: seq[int] = @[5, 3, 1, 8, 2, 10, 6, 4, 9, 7]

for _, el in randomSeq:
  discard randomRoot.insert(el)

discard singleRoot.insert(3)

inOrder(linearRoot)
inOrder(randomRoot)

echo "randomRoot value: ", randomRoot.data
echo "remove 5 from randomRoot: ", randomRoot.remove(5)
echo "printing randomRoot:"
randomRoot.inOrder

echo "singleRoot value: ", singleRoot.data
echo "remove singleRoot: ", singleRoot.remove(singleRoot.data)
echo "printing singleRoot:"
singleRoot.inOrder

# echo repr(linearRoot)
