import AsyncDispatch
import os
import httpclient
import json

const SCHW_data = "http://chart.finance.yahoo.com/table.csv?s=SCHW&a=0&b=8&c=2017&d=1&e=8&f=2017&g=d&ignore=.csv"
const AAPL_data = "http://chart.finance.yahoo.com/table.csv?s=AAPL&a=0&b=8&c=2017&d=1&e=8&f=2017&g=d&ignore=.csv"

const AVGO_summary = "http://query2.finance.yahoo.com/v10/finance/quoteSummary/AVGO?modules=defaultKeyStatistics%2CfinancialData%2CcalendarEvents"

# var client = newHttpClient()
# echo "Waiting for SCHW data ...\n"
# echo "SCHW: ", client.getContent(SCHW_data)

proc fetch(symbol: string) {.async.} =
  let url = "http://chart.finance.yahoo.com/table.csv?s=" & symbol & "&a=0&b=8&c=2017&d=1&e=8&f=2017&g=d&ignore=.csv"
  let asyncClient = newAsyncHttpClient()
  echo "Fetching ", symbol, " ..."
  let content = await asyncClient.getContent(url)
  writeFile(symbol & ".txt", content)

waitFor fetch("AVGO")
waitFor fetch("MSFT")
waitFor fetch("C")
waitFor fetch("SCHW")
waitFor fetch("BAC")

# https://nim-lang.org/docs/json.html

# let summaryStr = client.getContent(AVGO_summary)
# echo "AVGO: ", summaryStr

# # echo repr(jobj["quoteSummary"]["result"][0]["defaultKeyStatistics"]["maxAge"])

# let jobj = parseJson(summaryStr)
# assert(jobj.kind == JObject)
# echo jobj["quoteSummary"]["result"][0]["defaultKeyStatistics"]["enterpriseValue"]["fmt"]
