# void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
# {
#     int bpp = surface->format->BytesPerPixel;
#     /* Here p is the address to the pixel we want to set */
#     Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

#     switch(bpp) {
#     case 1:
#         *p = pixel;
#         break;

#     case 2:
#         *(Uint16 *)p = pixel;
#         break;

#     case 3:
#         if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
#             p[0] = (pixel >> 16) & 0xff;
#             p[1] = (pixel >> 8) & 0xff;
#             p[2] = pixel & 0xff;
#         } else {
#             p[0] = pixel & 0xff;
#             p[1] = (pixel >> 8) & 0xff;
#             p[2] = (pixel >> 16) & 0xff;
#         }
#         break;

#     case 4:
#         *(Uint32 *)p = pixel;
#         break;
#     }
# }

# proc putpixel(surface: SurfacePtr, x, y: int32, pixel: uint32) =
#     let bpp = int32 surface.format.BytesPerPixel
#     let p = cast[ptr uint8](cast[int](surface.pixels) + y * surface.pitch + x * bpp)

#     case bpp
#     of 1: p[] = pixel.uint8
#     of 2: cast[ptr uint16](p)[] = pixel.uint16
#     of 4: cast[ptr uint32](p)[] = pixel