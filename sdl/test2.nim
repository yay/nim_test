import sdl2, sdl2/image
import math
import times
import strutils

const
  width = 800
  height = 800

var
  win: WindowPtr
  ren: RendererPtr
  event: Event

type
  Color = object
    # r, g, b, a: range[0..255]
    r, g, b, a: uint8

# discard sdl2.init(INIT_EVERYTHING)
discard sdl2.init(INIT_VIDEO)

win = createWindow("Hello World!", 100, 100, width, height, SDL_WINDOW_SHOWN)
if win == nil:
  echo "createWindow Error: ", getError()
  quit(1)

# surface = getSurface(win)
# if surface == nil:
#     echo "getSurface Error: ", getError()
#     quit(1)

ren = createRenderer(win, -1, RendererAccelerated or RendererPresentVsync)
if ren == nil:
  echo "createRenderer Error: ", getError()
  quit(1)

proc toZeroOne(n: float): float {. inline .} =
  result = max(0, min(1, n))

proc render(t: float64, px, py: int): Color =
  let u0 = (px.float64 / width) * 2 - 1
  let u1 = (py.float64 / width) * 2 - (height.float64 / width.float64)
  let tt: float64 = sin(t / 8) * 64
  var x: float64 = u0 * tt + sin(t * 2.1) * 4
  var y: float64 = u1 * tt + cos(t * 2.3) * 4
  var c: float64 = sin(x) + sin(y)
  let zoom: float64 = sin(t)
  x = x * zoom * 2 + sin(t * 1.1)
  y = y * zoom * 2 + cos(t * 1.3)
  let xx: float64 = cos(t * 0.7) * x - sin(t * 0.7) * y
  let yy: float64 = sin(t * 0.7) * x + cos(t * 0.7) * y
  c = (sin(c + (sin(xx) + sin(yy))) + 1) * 0.4
  var v: float64 = 2 - sqrt(u0 * u0 + u1 * u1) * 2

  let r = (v * (c + v * 0.4)).toZeroOne
  let g = (v * (c * c - 0.5 + v * 0.5)).toZeroOne
  let b = (v * (c * 1.9)).toZeroOne

  result.r = uint8(255.99 * r)
  result.g = uint8(255.99 * g)
  result.b = uint8(255.99 * b)
  result.a = 255

var startTime = epochTime()
var counter = 0

while true:
  let time = epochTime() - startTime * 0.2
  for x in 0..width-1:
    for y in 0..height-1:
      let color = render(time, x, y)
      setDrawColor(ren, color.r, color.g, color.b, color.a)
      drawPoint(ren, x.int32, y.int32)

  ren.present()

  if pollEvent(event) and event.kind == QuitEvent:
    break
  win.setTitle(intToStr(counter))
  inc(counter)

# while true:
#   for x in 0..width:
#     for y in 0..height:
#       let r = uint8(x.float64 / width.float64 * 255.99)
#       let g = uint8(y.float64 / width.float64 * 255.99)
#       setDrawColor(ren, r, g, 0, 255)
#       drawPoint(ren, x.int32, y.int32)

#   ren.present()

#   if pollEvent(event) and event.kind == QuitEvent:
#     break


# ren.present()


ren.destroy()
win.destroy()

sdl2.quit()
