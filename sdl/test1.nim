import sdl2, sdl2/image

# https://hookrace.net/blog/writing-a-2d-platform-game-in-nim-with-sdl2/
# https://github.com/nim-lang/sdl2

# brew install sdl2{,_gfx,_image,_mixer,_net,_ttf}
# nimble install sdl2 winim strfmt

var
  win: WindowPtr
  ren: RendererPtr
  texture: TexturePtr

discard sdl2.init(INIT_EVERYTHING)

win = createWindow("Hello World!", 100, 100, 960, 540, SDL_WINDOW_SHOWN)
if win == nil:
  echo("createWindow error: ", getError())
  quit(1)

ren = createRenderer(win, -1, RendererAccelerated or RendererPresentVsync)
if ren == nil:
  echo("createRenderer error: ", getError())
  quit(1)

texture = loadTexture(ren, "image.png")
if texture == nil:
  echo("loadTexture error: ", getError())
  quit(1)

discard ren.clear()
discard ren.copy(texture, nil, nil)
ren.present()
delay(5000)

destroy(texture)
destroyWindow(win)

sdl2.quit()