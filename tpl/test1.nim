# http://rnduja.github.io/2015/10/21/scientific-nim/

import sequtils

type Point = tuple[x, y: float]

proc `+`(p, q: Point): Point = (p.x + q.x, p.y + q.y)

proc `/`(p: Point, k: float): Point = (p.x / k, p.y / k)

proc average(points: seq[Point]): Point =
  foldl(points, a + b) / float(points.len)


# 125 + 5 + 8 stock
# 130 + 5 stock